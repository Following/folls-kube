#!/bin/bash

IFS=', ' read -r -a addresses <<< "$MONGO_URLS"

addresses_config_strings=()
for i in "${!addresses[@]}"
do
    address=${addresses[$i]}
    addresses_config_strings+=('{_id: '${i}', host: '"'"${address}"'"'}')
    
    while ! nc -w 1 ${address} 27017; do
        sleep 0.1
        echo 'resolving '${address}
    done
done

echo resolved, running replSetInitiate

function join_by { local IFS="$1"; shift; echo "$*"; }
config_members=$(join_by , "${addresses_config_strings[@]}")

cfg="{
    _id: 'rs0',
    members: [
        "${config_members}"
    ]
}"

mongo ${addresses[0]}:27017 --eval "JSON.stringify(db.adminCommand({'replSetInitiate' : $cfg}))"

exit 0
